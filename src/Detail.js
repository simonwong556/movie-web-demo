import React, { Component } from 'react';
import './App.css';
import axios from 'axios'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Moment from 'react-moment';
import LinesEllipsis from 'react-lines-ellipsis'
import StarRatingComponent from 'react-star-rating-component';
import Carousel from 'nuka-carousel'

import Header from './header/Header'

class Detail extends Component {
  constructor(props) {
    super(props)
    this.state = {
        maxLine: '4',
        movie: {}
    };
  }

  componentDidMount(  ) {
    var movieId = this.props.match.params.id
    axios.get('https://api.hkmovie6.com/hkm/movies/'+movieId)
    .then(response => {
        this.setState ({
            movie: response.data
        })
      }
    )
  }

  handleReflow () {
    this.setState ({
        maxLine: '100'
    })
  }

  render() {
    var detail = this.state.movie
    var infoData,rating,favCount,commentCount,silder, silderAll, silderVideo
    var silderSetting = {
        dots: true,
    };
    if ( detail.screenShots ) {
        silderVideo = detail.multitrailers.map((shot, key)=> {
            return  <div key={key.toString()} ><iframe className="Detail-silder-video" src={shot}/></div>
        })
        silder = detail.screenShots.map((shot, key)=> {
            return  <div key={key.toString()} ><img className="Detail-silder-img" src={shot}/></div>
        })
        silderAll = <Carousel dragging={false} className="Detail-silder" slidesToShow={1} cellAlign="center">{silderVideo}{silder}</Carousel>
    }

    if (detail.rating) {
        rating = <div className="Detail-count-data-list">{(detail.rating/100).toFixed(1)}</div>
    } else {
        rating = <div className="Detail-count-data-list">- -</div>
    }
    if (detail.favCount) {
        favCount = <span className="App-count-data">{detail.favCount}</span>
    } else {
        favCount = <span className="App-count-data">0</span>
    }
    if (detail.commentCount){
        commentCount = <span className="App-count-data">{detail.commentCount}</span>
    } else {
        commentCount = <span className="App-count-data">0</span>
    }
    if ( detail.id != undefined ) {
        infoData =
            <div className="App-body">
                <Container>
                    <Row><Col xs={12}>{silderAll}</Col></Row>
                    <br/><Row>
                        <Col className="Detail-info-synopsis" xs={4}>{rating}
                            <StarRatingComponent name="rate1"  starCount={5} value={detail.rating/100}/>
                        </Col>
                        <Col className="Detail-info-synopsis Detail-info-synopsis-1" xs={8}>
                            <div>{detail.name}</div>
                            <div className="Detail-info-count">
                                <span>{favCount} </span> <span> {commentCount}</span>
                            </div>
                            <div className="Detail-info-other">
                                <span><Moment format="YYYY/MM/DD">
                                    {detail.openDate} 
                                </Moment></span><span className="Detail-info-other-mid"> | </span>
                                {detail.infoDict.Duration} mins<span className="Detail-info-other-mid"> | </span>
                                {detail.infoDict.Category}
                            </div>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col className="Detail-info-synopsis" xs={12}>
                            <LinesEllipsis text={detail.synopsis} maxLine={this.state.maxLine} ellipsis='... see More' trimRight basedOn='words' onClick={this.handleReflow.bind(this)}/>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col className="Detail-info-dict" xs={3}><span>Director</span></Col><Col xs={9}><span>{detail.infoDict.Director}</span></Col>
                        <Col className="Detail-info-dict" xs={3}><span>Cast</span></Col><Col xs={9}><span>{detail.infoDict.Cast}</span></Col>
                        <Col className="Detail-info-dict" xs={3}><span>Genre</span></Col><Col xs={9}><span>{detail.infoDict.Genre}</span></Col>
                        <Col className="Detail-info-dict" xs={3}><span>Language</span></Col><Col xs={9}><span>{detail.infoDict.Language}</span></Col>
                    </Row>
                </Container>
            </div>
    }
    return (
        <div className="Detail">
        <Header type={'Movie Info'}></Header>
            {infoData}
        </div>
    )
  }
}

export default Detail;
