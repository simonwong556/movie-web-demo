import React from 'react';
import './Header.css';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const Header = (props) => {
  var topBarRight
  if ( props.type==='Movie' )  {
    topBarRight = <Col xs={2} className="Header-listType-btn" onClick={props.changeEvent}>Click</Col>
  } else {
    topBarRight = <Col xs={2} className="Header-listType-btn" >Share</Col>
  }
  return (
    <Row className="Header-body">
      <Col xs={2}></Col>
      <Col xs={8} className="Header-title">{props.type}</Col>
      {topBarRight}
    </Row>
  );
}

export default Header;