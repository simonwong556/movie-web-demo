import React, { Component } from 'react';
import './App.css';
import axios from 'axios'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Moment from 'react-moment';
import StarRatingComponent from 'react-star-rating-component';

import Header from './header/Header'
import {Link} from 'react-router-dom'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listType: 'grid',
      movies: []
    };
  }

  componentDidMount() {
    axios.get('https://api.hkmovie6.com/hkm/movies?type=showing')
    .then(response => {
        this.setState ({
          movies: response.data
        })
      }
    )
  }

  changeViewList () {
    if (this.state.listType == 'grid') {
      this.setState ({
        listType: 'list'
      })
    } else {
      this.setState ({
        listType: 'grid'
      })
    }
  }

  render() {
    let listItems = this.state.movies.map((movie, key)=> {
      var rating, favCount, commentCount
      var rowNum = 0
      rowNum = movie.rating ? ++rowNum : rowNum
      rowNum = movie.favCount ? ++rowNum : rowNum
      rowNum = movie.commentCount ? ++rowNum : rowNum
      if (!movie.thumbnail) {
        return ''
      }
      if ( this.state.listType == 'grid' ) {
        if (movie.rating) {
          rating = <Col xs={12/rowNum} className="App-count-data">{(movie.rating/100).toFixed(1)}</Col>
        }
        if (movie.favCount) {
          favCount = <Col xs={12/rowNum} className="App-count-data">{movie.favCount}</Col>
        }
        if (movie.commentCount){
          commentCount = <Col xs={12/rowNum} className="App-count-data">{movie.commentCount}</Col>
        }

        return (
            <Col xs={4} md={3} key={movie.id}>
              <Link to={{pathname:'/detail/'+movie.id}}>
                <img className="App-thumbnail" src={movie.thumbnail}/>
                <div className="App-title"><span>{movie.name}</span></div>
                <Container className="App-count">
                  <Row className="App-count-row">{rating}{favCount}{commentCount}</Row>
                </Container>
              </Link>
            </Col>
        );
      } else {
        if (movie.rating) {
          rating = <div className="App-count-data-list">{(movie.rating/100).toFixed(1)}</div>
        } else {
          rating = <div className="App-count-data-list">- -</div>
        }
        if (movie.favCount) {
          favCount = <span className="App-count-data">{movie.favCount}</span>
        } else {
          favCount = <span className="App-count-data">0</span>
        }
        if (movie.commentCount){
          commentCount = <span className="App-count-data">{movie.commentCount}</span>
        } else {
          commentCount = <span className="App-count-data">0</span>
        }
        return (
          <Col xs={12} key={movie.id} className="App-list-top">
            <div className="App-list">
              <Link to={{pathname:'/detail/'+movie.id}}>
                <img className="App-thumbnail-list" src={movie.thumbnail}/>  
                <Row className="App-count-row">
                  <Col xs={12} className="App-list-detail">
                    <div className="App-title-list">
                    <Row className="App-count-row">
                        <Col className="removeRightPad" xs={4}>{rating}
                          <StarRatingComponent name="rate1" starCount={5} value={movie.rating/100}/>
                        </Col>
                        <Col xs={8} className="App-list-detial">
                          <div><span>{movie.name}</span></div>
                          <div className="App-list-detial-count"><span>{favCount}</span> <span>{commentCount}</span></div>
                          <div className="App-list-detial-date"><span><Moment format="YYYY/MM/DD">{movie.openDate} </Moment></span></div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </Link>
            </div>
          </Col>
        );
      }
    });

    return (
      <div className="App">
        <Header type={'Movie'} changeEvent={this.changeViewList.bind(this)}></Header>
        <div className="App-body">
          <Row>
            {listItems}
          </Row>
        </div>

      </div>
    );
  }
}

export default App;

